#!/bin/bash

############################################################
#
# A bash script which can be used as a template.
#
#

_basedir="$(dirname ${BASH_SOURCE})"
_script=$(basename $0 | sed 's/.sh//g')


##############################
# Default variables
##############################

_object="World"                         # default object value

_trap="true"                            # trap Ctrl-c user input
_log="true"                             # append message to a logfile
_temp="true"                            # Create a temporary work folder
_pid="true"                             # allow only one instance to run

_tempdir="/var/tmp"                     # relative workfolder
_tempdir_minsize=10                     # minimum size available in MB

_pidfile="${_basedir}/${_script}.pid"   # location and name of pid file

_logdir="${_basedir}/logs"              # location and name of log file
_logfile=${_logdir}/${_script}.log      # name of logfile
_logfile_maxsize=1024                   # maximum size of log in KB
_logretention=3                         # retention days of logfiles

_inifile="${_basedir}/${_script}.ini"   # configuration file


##############################
# Functions
##############################

function _loadini()
{
    _section=$1
    if [[ -z ${_section} ]]
    then
        _msg fatal "No section name given."
    fi

    if [[ -f ${_inifile} ]]
    then
        if [[ -z $(grep "^\[${_section}\]" ${_inifile}) ]]
        then
            _msg fatal "Section ${_section} not found in ini file."
        else
            _tmpcfg=$(mktemp -p ${_tempdir})
            awk '/\[/{prefix=$0; next} $1{print prefix $0}' ${_inifile} \
                | sed 's/ *=* /=/g'         \
                | grep "^\[${_section}\]"   \
                | cut -d']' -f2             \
                | grep -v '^ *#'            \
                | sed s'/^/_/g'             \
                > ${_tmpcfg}
            source ${_tmpcfg} || _msg fatal "Syntax error in ini file."
            rm -f ${_tmpcfg}
        fi
    fi
    _section=''
}

function _mkpid()
{
    if [[ -f ${_pidfile} ]]
    then
        ps -p `cat ${_pidfile}` > /dev/null
        if [[ $? -ne 0 ]]
        then
            _msg warn "Found stale pid file."
        else
            _msg warn "Script is already running."
            exit 2
        fi
    fi
    echo $$ > ${_pidfile}
    if [[ $? -ne 0 ]]
    then
        _msg fatal "Could not create pidfile."
    fi
}

function _rmpid()
{
    if [[ -f ${_pidfile} ]]
    then
        rm -f ${_pidfile}
    else
        _msg warn "Pidfile already deleted!?"
    fi
}

function _chktemp()
{
    let _tempdir_minsize=${_tempdir_minsize}*1024
    if [[ $(df ${_tempdir} | awk 'NR==2{print $4}') -lt ${_tempdir_minsize} ]]
    then
        _msg fatal "Not enough diskspace to create a temp folder."
    fi
}

function _mktemp()
{
    _tmpdir=$(mktemp -d -p ${_tempdir})
    if [[ ! -w ${_tmpdir} ]]
    then
        _msg fatal "Could not write to temp folder ${_tmpdir}"
    fi
}

function _rmtemp() {
    [[ ${#_tmpdir} -le 1 ]] && return 1
    if [[ -d ${_tmpdir} ]]
    then
        rm -f ${_tmpdir}/*~
        rmdir ${_tmpdir}
        if [[ $? -gt 0 ]]
        then
            _msg warn "Could not cleanup and remove tempfolder ${_tmpdir}."
        fi
    else
        _msg warn "Temp folder already deleted!?"
    fi
}

function _aborting()
{
    printf "Aborting...\n"
    _rmtemp
    _rmpid
    exit 2
}

function _logrotate()
{
    if [[ -f ${_logfile} ]]
    then
        _logsize=$(du -b ${_logfile} | awk -F' ' '{print $1}')
        let _logsize=${_logsize}/1024
        if [[ ${_logsize} -gt ${_logfile_maxsize} ]]
        then
          _msg info "Logfile reached maximum size. Initiating log rotation."
          c=1
          _datenow=$(date +'%Y-%m-%d')
          while [[ $c -lt 99999 ]]; do
              if [[ ! -f ${_logfile}-${_datenow}.$c.gz ]]
              then
                  mv ${_logfile} ${_logfile}-${_datenow}.$c
                  gzip ${_logfile}-${_datenow}.$c
                  break
              fi
              let c=$c+1
          done
          true > ${_logfile}
        fi
    fi
}

function _logcleanup()
{
    [[ ${#_logdir} -le 1 ]] && exit 1

    if [[ ! -d ${_logdir} ]]
    then
        mkdir -p ${_logdir} || exit 1
        _msg log "Folder ${_logdir} created for logfiles."
    fi

    _count=$(find ${_logdir} -mtime +${_logretention} -name "${_script}.log-*" | wc -l)

    if [[ ${_count} -gt 0 ]]
    then
        _msg info "Deleting ${_count} logfiles older than ${_logretention} days."

        find ${_logdir} -mtime +${_logretention} \
           -name "${_script}.log-*" -exec rm -f {} \;
    fi
}

function _msg()
{
    [[ $1 == 'info' ]]  && printf "${_cblue}[$1]${_cend} $2\n"
    [[ $1 == 'warn' ]]  && printf "${_cyellow}[$1]${_cend} $2\n"
    [[ $1 == 'fatal' ]] && printf "${_cred}[$1]${_cend} $2\n"

    if [[ ${_log} =~ t ]]
    then
        _logrotate
        _logcleanup

        if [[ ! -d ${_logdir} ]]
        then
            mkdir -p ${_logdir} || exit 1
        fi

        printf "$(date +'%d/%b/%Y %H:%M:%S') $(hostname) [$1]: $2\n" >> ${_logfile}
    fi
    if [[ $1 == 'fatal' ]]
    then
        _aborting
    fi
}

function _createfile()
{
    cat > ${_tmpdir}/helloworld~ << EOL
Hello ${_object}!
This is a sample.
EOL
}

function _display()
{
    _msg log "Displaying the contents of file; object=${_object}"
    cat -n ${_tmpdir}/helloworld~
}

_spinner()
{
    set +x; c=1
    printf '\n'
    local -a marks=( '/' '-' '\' '|' )
    while [[ $c -lt 30 ]]
    do
        printf "\b%s" "${marks[i++ % ${#marks[@]}]}"
        sleep 0.1
        let c=$c+1
    done
    printf '\n'
    [[ ${_verbose} -gt 0 ]] && set -x
}

##############################
# Color codes
##############################

_cred="\e[31m"
_cgreen="\e[32m"
_cblue="\e[94m"
_cyellow="\e[33m"
_cend="\e[0m"


##############################
# Help and parameters
##############################

#
# Show usage options.
#
function _usage()
{
    cat << EOL
Usage: $0 [OPTION]...
Example of a bash script generating a file and displaying it contents.

Available options:
  -o, --object WORD             Replace World with some other object. (default=World)
  -s, --section WORD            Load variables under given section in ini file.
  -v, --verbose                 Display verbose information.
  -h, --help                    Display this help message.

EOL
    exit 2
}


#
# Read main configuration
#
_loadini main

#
# Convert long parameters.
#
for arg in "$@"; do
    shift
    case "$arg" in
        '--verbose') set -- "$@" '-v'   ;;
        '--help')    set -- "$@" '-h'   ;;
        '--object')  set -- "$@" '-o'   ;;
        '--section') set -- "$@" '-s'   ;;
        *)           set -- "$@" "$arg" ;;
     esac
done

#
# Get the options.
#
while getopts :o:s:hv opt; do
    case ${opt} in
        v) _verbose=1  ;;
        h) _usage   ;;
        s) _section=${OPTARG} ;;
        o) _object=${OPTARG}  ;;
        *)
           printf "Invalid Option: $1.\n"
           _usage;;
    esac
done

#
# Enable verbose
#

[[ ${_verbose} -gt 0 ]] && set -x

#
# When only one parameter were given.
#
if [[ "$#" -eq 1 && $1 != \-* ]]; then
    _object=$1
fi

#
# Load a specified section in ini file.
#
if [[ -n ${_section} ]]
then
    _loadini ${_section}
fi

##############################
# Input validation
##############################

#
# Do not allow word "Planet".
#
if [[ ${_object} =~ [Pp]lanet ]]
then
    _msg fatal "The word ${_object} is not allowed here.\n"
fi

#
# Check the length of the word.
#
if [[ ${#_object} -lt 3 ]]
then
    _msg fatal "The word ${_object} is too short.\n"
fi


##############################
# Script
##############################

#
# Set the (ctrl-c) trap handler.
#
[[ ${_trap} =~ t ]] && trap '_aborting' SIGINT

function _main {

    if [[ ${_pid} =~ t ]]
    then
        _mkpid
    fi

    if [[ ${_temp} =~ t ]]
    then
        _chktemp
        _mktemp
        _createfile
        _display
    else
        printf "Not much to do without a temp folder :-(\n"
    fi
    _spinner
    _rmtemp

    if [[ ${_pid} =~ t ]]
    then
        _rmpid
    fi
}

_main

exit 0
