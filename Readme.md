
# Bash Template

Nice and clean bash script example including many used functions like creating
a temporary folder.

The template includes:

- default variables
- basic functions
- parameters options
- validation checks
- main script


